package com.camelpack.amqrouter;

import org.apache.camel.builder.RouteBuilder;

public class AMQ2FSORouteBuilder extends RouteBuilder {

	//configure route for jms2fso component
    @Override
    public void configure() throws Exception {
        from("jms:queue:TesQ").split().tokenize("\n").to("file:/msgbox/Out");
    }

}
