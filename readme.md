# Build Docker images

Initially this project will build two images, one for Fuse another for Active MQ
If some images already exist some clean-up should be performed first.

To create a new version of Camel route recompile and repackage the Camel apps from CamelsHerder 

    mvn clean package -X

After that from the same subfolder assemble the images 

    vagrant up --no-parralel


Check the status of built containers

    docker ps -a    

Output should be similar to:

    CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                              NAMES
    d037e8b00471        293b0a5172a1        "/bin/sh -c '/opt/..."   13 minutes ago      Exited (137) 56 seconds ago         jboss-fuse-full
    ae84ad8efe87        8f97f98e940d        "/bin/sh -c 'bin/a..."   25 minutes ago      Exited (137) 36 seconds ago         active-camel-queue

## Docker clean-up 

Images:

    sudo docker rmi -f $(docker images -a -q)

Containers:

    sudo docker rm  -f $(docker ps -a -q)

## Manual starting and linking containers

In this case all containers are stopped (see the status above, use the actual IDs from your images). 
Start Active MQ container first 

    docker run -p 8161:8161 -p 1099:1099 --name camel-active-queue <image_id>

Like 

    docker run -p 8161:8161 -p 1099:1099 --name camel-active-queue 8f97f98e940d


After that start Fuse container and link it to ActiveMQ

    docker run -it -p 8181:8181 -p 8080:8080 --name camel-herder --link camel-active-queue:camelamq <image_id>

Like

    docker run -it -p 8181:8181 -p 8080:8080 --name camel-herder --link camel-active-queue:camelamq 293b0a5172a1

Check the status of the running containers docker ps. If they are in running state consoles will be available

Fuse:     http://localhost:8181/hawtio/
ActiveMQ: http://localhost:8161/admin/queues.jsp

Test messages can be sent using test client CamelHTTPLoader or any other HTTP Client, like RESTED to the URL:

 http://localhost:8080/services/HTTPAcceptor

Messages will be visible in the logs (Fuse) and all consoles
  