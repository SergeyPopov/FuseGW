package main.java.no.vps.adapters.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class Config {
  
	private static Logger logger = Logger.getLogger(
            "main.java.no.vps.adapters.config");
	
	public static  boolean IS_LOCAL = true;
	public static  String LOGGING_LEVEL = "3";	
    public static  String XML_EXT           = "ATKSTAT";
    public static  String XMLLOG            = "xml-parser.log";
    public static  int    XMLPARSER_TIMEOUT = 10;
	//"D:\Sergey\Projects\Common\MCSA\JavaAFComponents\MSGBox\TP";
    //"D:/Sergey/Projects/Common/MCSA/JavaAFComponents/MSGBox/TP";
    public static  String XMLDIR = "/msgbox/In/";
    public static  String XMLARCDIR = "/msgbox/Archive/";
    public static  String XML_PARSESUBDIR = "false";
    public static  String XMLFILE = null;
    public static  String XMLENCODING = "ISO-8859-1";
    public static  String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static  String DB_NAME =  "dlake";
	public static  String UserName = "root";
	public static  String Password = "admin";
    public static  String POST_PROJECT_URL = "http://localhost:8181/hawtio/"; 
    public static  String ES_LOCALHTTP_URL = "http://camelbroker:8080/services/HTTPAcceptor";
    public static  String PROJECT_DB_URL = null;
    public static  String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static  String OBJPARSINGPATTERN = "SEPARATE";
    public static  String LAST_PROCESSED_FILE = null;

    //===============================================================================
    
    public static  String FF_PARSESUBDIR   = "false";
    public static  String FF_EXT           = "STA";
    public static  String FFLOG        = "ff-parser.log";
    public static  int    FFPARSER_TIMEOUT = 10;
    public static  String FFDIR  = "/msgbox/In/";
    public static  String FFFILE = null;
    public static  String FFTRACE  = null;
    
    
    public static void getParams(){
    
    	Properties prop = new Properties();
    	String propfile ="./config/fso.properties";
    	//String propfile ="fso.properties";
    	
    	logger.info("TESTLOADER config file  : " + propfile);

    	File fp         = new File(propfile);
    	//BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(propfile)));
    	
    	try {

    		prop.load(new FileInputStream(fp));

    		// set the properties value
    		LOGGING_LEVEL       = prop.getProperty("logging_level");
    		XML_PARSESUBDIR     = prop.getProperty("xml_parsesubdir");
    		XML_EXT             = prop.getProperty("xml_ext");
    		XMLDIR              = prop.getProperty("xmldir");
    		XMLARCDIR           = prop.getProperty("xmlarcdir");
    		XMLFILE             = prop.getProperty("xmlfile"); 
    		XMLLOG              = prop.getProperty("xmllog"); 
    		XMLENCODING         = prop.getProperty("xmlencoding");    	
    		DATE_FORMAT         = prop.getProperty("date_format");        		
    		DB_NAME             = prop.getProperty("db_name");    
    		UserName       	    = prop.getProperty("username");        		
    		Password            = prop.getProperty("password");      		
    		POST_PROJECT_URL    = prop.getProperty("post_project_url");      		
    		ES_LOCALHTTP_URL    = prop.getProperty("es_localhttp_url");  
    		PROJECT_DB_URL      = prop.getProperty("project_db_url");     		
    		JDBC_DRIVER         = prop.getProperty("jdbc_driver");     
    		OBJPARSINGPATTERN   = prop.getProperty("objparsingpattern");        		
    		LAST_PROCESSED_FILE = prop.getProperty("last_processed_file");
    		
    		
    	} catch (IOException io) {
    		io.printStackTrace();
    	} 
   }
  
    public static String getLastParsedFile(String atk_dir){
        
    	Properties prop = new Properties();
    	String propfile = FFTRACE;
    	File fp         = new File(atk_dir+propfile);
    	String fname    = null;
    	
    	try {
    		prop.load(new FileInputStream(fp));
    		// set the properties value
    		fname  = prop.getProperty("last_processed_file");
    	}catch (FileNotFoundException e){
    	    return fname;
    	}catch (Exception io) {
    		io.printStackTrace();
    	} 
    	return fname;
   }
    
    
    public static void setParams(String parameter) throws FileNotFoundException{
        
    	Properties prop = new Properties();
    	//OutputStream output = null;
    	String propfile ="stat.properties";
    	File fp         = new File(propfile);
    	FileOutputStream fileOut = new FileOutputStream(fp);
    	
    	try {

    		//output = new FileOutputStream(fp);

    		// set the properties value
    		prop.setProperty("last_processed_file", parameter);

    		// save properties to project root folder
    		prop.store(fileOut, null);

    	} catch (IOException io) {
    		io.printStackTrace();
    	} finally {
    		if (fileOut != null) {
    			try {
    				fileOut.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}

    	}
   }    
    
    public static void main (String args[]) throws IOException{
    	//setParams("aaaaaaaaaa");
    	
  	   try{
  		   Config.getParams();
  		   System.out.println("All parameters are set");
  		   System.out.println("XMLDIR: "+XMLDIR);
  	   }catch(Exception e){
  		  e.printStackTrace();
  	   }
       
	}
}

