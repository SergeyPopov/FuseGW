package main.java.no.vps.adapters.http;


//import utils.xml.XMLHelper;
//import utils.xml.XMLHttp;

import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

//import oracle.xml.parser.v2.XMLDocument;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


public class PostHelper 
{

  private static final String srvgXDIUrl = "http://camelbroker:8080/services/HTTPAcceptor";

  public PostHelper()
  {
  }
   
  /*
   public void deliver(java.io.Reader reader) throws IOException, SAXParseException, SAXException
   {
    XMLDocument docToPost = XMLHelper.parse(reader,null);
    String url = srvgXDIUrl;
    URL target = new URL(url);
    XMLHttp http = new XMLHttp();
    XMLDocument response = XMLHttp.doPost(docToPost,target);
    response.print(System.out);
   }
   */
  
   public void deliverPost(java.io.Reader reader) 
          throws IOException, SAXParseException, SAXException
    // Parse XML message in a string, No external references so null BaseURL ok
   {
    InputStream xmlin =null;
    String url = null;
    
    java.io.BufferedReader bodyReader = new java.io.BufferedReader(reader);
    StringBuffer sBuf = new StringBuffer();
    String tmp;
    while((tmp = bodyReader.readLine()) != null )
    {
      sBuf.append(tmp);
    }

    //Get HTTP client
    HttpClient httpclient = new HttpClient();
    
    /* 
    httpclient.getState().setCredentials(
        new HttpAuthRealm(srvgXDIUrl, "gXDI"),
        new UsernamePasswordCredentials( username, password ) );
    */
    
    //Prepare HTTP post
    //PostMethod post = new PostMethod(srvgXDIUrl);
   PostMethod post = new PostMethod(url);
   
    //post.setDoAuthentication(true);
   
    post.setFollowRedirects(true);
    post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
    
    byte[] content = sBuf.toString().getBytes("ISO-8859-1");
    post.setRequestBody(new ByteArrayInputStream(content));
            
    //Per default, the request content needs to be buffered
    // in order to determine its length.
    // Request body buffering can be avoided when
    // = content length is explicitly specified
    // = chunk-encoding is used

    if (content.length < Integer.MAX_VALUE) {
      //post.setRequestEntity.setContentLength((int)content.length);
      post.setRequestContentLength((int)content.length);
      } else {
         post.setContentChunked(true);
         //post.setRequestContentLength(EntityEnclosingMethod.CONTENT_LENGTH_CHUNKED);
      }
   }
   
   public void deliverHttpPost(java.io.Reader reader) throws IOException
  {
    String server = srvgXDIUrl;
    try {
      // Connect to the server
      URL u = new URL(server);
      URLConnection uc = u.openConnection();
      HttpURLConnection connection = (HttpURLConnection) uc;
      connection.setDoOutput(true);
      connection.setDoInput(true); 
      connection.setRequestMethod("POST");
      OutputStream out = connection.getOutputStream();
      Writer wout = new OutputStreamWriter(out , "UTF-8");
      
      java.io.BufferedReader bodyReader = new java.io.BufferedReader(reader);
  
     // Write the request
      String line = bodyReader.readLine();
         while( line != null ){
                wout.write(line);
                line = bodyReader.readLine();
         }
      wout.flush();
      out.close();
      bodyReader.close();
     
      // Read the response
      /*
      InputStream in = connection.getInputStream();
      int c;
      while ((c = in.read()) != -1) System.out.write(c);
      System.out.println();
     
      ------------ 
      InputStream in = connection.getInputStream();
      BigInteger result = readEndPointXMLAcknowledge(in);
      System.out.println(result);
      in.close();
     */
      connection.disconnect();
    }
    catch (IOException e) {
      System.err.println(e); 
    }
  }


   public int deliverHttpPost(String file2post, String Endpoint) throws IOException
  {
    String server = Endpoint;
    int deliveristatus = 1;
    try {
      // Connect to the server
      URL u = new URL(server);
      URLConnection uc = u.openConnection();
      HttpURLConnection connection = (HttpURLConnection) uc;
      connection.setDoOutput(true);
      connection.setDoInput(true); 
      connection.setRequestMethod("POST");
      connection.setRequestProperty("Content-type", "text/xml; charset=utf-8");
      OutputStream out = connection.getOutputStream();
      Writer wout = new OutputStreamWriter(out , "UTF-8");
  
     // Write the request
      java.io.Reader reader = new FileReader(file2post);
      java.io.BufferedReader bodyReader = new java.io.BufferedReader(reader);
      
      // Write the request
       String line = bodyReader.readLine();
          while( line != null ){
                 wout.write(line);
                 line = bodyReader.readLine();
          }
       wout.flush();
       out.close();
       bodyReader.close();
     
      // Read the response
      InputStream in = connection.getInputStream();
      int c;
      while ((c = in.read()) != -1) System.out.write(c);
      System.out.println();

      
      //----
      /*
      InputStream in = connection.getInputStream();
      String result = readEndPointXMLAcknowledge(in);
      
      if (result.indexOf("MessageStatus>3</MessageStatus")!=-1){
         deliveristatus = 0;
      }
      else{
         deliveristatus = 1;
         System.out.println("Acknowledge: " + result);
      }
      */   
      //----
      
      in.close();
      connection.disconnect();
    }
    catch (IOException e) {
      System.err.println(e); 
    }
    return deliveristatus;
  }
 
 /*
  private static BigInteger readEndPointXMLAcknowledge(InputStream in) 
  throws IOException, NumberFormatException,StringIndexOutOfBoundsException
  {
    
    StringBuffer sb = new StringBuffer();
    Reader reader = new InputStreamReader(in, "ISO-8859-1");
    int c;
    while ((c = in.read()) != -1) sb.append((char) c);
    
    String document = sb.toString();
    String startTag = "<Acknowledge>";
    String endTag   = "</Acknowledge>";
    int start = document.indexOf(startTag) + startTag.length();
    int end = document.indexOf(endTag);
    String result = document.substring(start, end);
    return new BigInteger(result);
    
  }  
*/
  private static String readEndPointXMLAcknowledge(InputStream in) 
  throws IOException, NumberFormatException,StringIndexOutOfBoundsException
  {
    
    StringBuffer sb = new StringBuffer();
    Reader reader = new InputStreamReader(in, "ISO-8859-1");
    int c;
    while ((c = in.read()) != -1) sb.append((char) c);
    
    String document = sb.toString();
    String startTag = "<Acknowledge>";
    String endTag   = "</Acknowledge>";
    int start = document.indexOf(startTag) + startTag.length();
    int end = document.indexOf(endTag);
    String result = document.substring(start, end);
    reader.close();

    return result;
    
  }  


}