2.2.1. List Vagrant VM 
sudo vagrant list 

2.2.1. Clean-Up using Vagrant
sudo vagrant destroy 

2.2.2. Clean-Up inactive Docker containers
sudo docker rmi -f $(docker images -a -q)
sudo docker rm  -f $(docker ps -a -q)

2.2.3. Dockers isfrastructure complete Clean-Up 
docker system prune
docker container prune

2.2.4. Prepare containers
sudo vagrant up –-no-parallel

2.2.5. Check containers statuses
sudo vagrant globas-status
sudo docker ps -a

2.2.6. Get  docker network(IP) settings
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <container_id>

2.2.7. Get  docker logs (individual)
Sudo docker logs <container_id>

2	3. Storing the Artifact 
3.1 Committing created container, as preparation for push to the repository
docker commit <container_id> sergeypopov/<container-name>

3.2 Tagging image for repository
docker tag <image_id> sergeypopov/<container-name>

3.3 Logging to the repository
docker login --username=<your_username> --email=<your_email>

3.4 Pushing image to the repository
docker push sergeypopov/<container-name>

3.5 Azure: Summary of the same operations for the Azire repository (use maroom repository user credentials )
docker login <company-name>.azurecr.io 
docker tag <image_id> <company-name>.azurecr.io/<container-name>
docker push <company-name>.azurecr.io/<container-name>


AMQ

docker run -p 8161:8161 -p 1099:1099 <image_id>
docker run -p 8161:8161 -p 1099:1099 --name camel-active-queue <image_id>



Fuse

docker run -it -p 8181:8181 <image_id>


run and link

docker run -it -p 8181:8181 --link active-camel-queue:camelamq <image_id>

docker run -it -p 8181:8181 --link active-camel-queue:camelamq 6cba88f61844



ie docker run -it -p 8181:8181 -p 8080:8080 --name camel-herder --link camel-active-queue:camelamq 976404a73b26

where camel-active-queue is the name of previously started AMQ container